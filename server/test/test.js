"use strict";

const Devebot = require("devebot");
const chores = Devebot.require("chores");
const envcloak = require("envcloak").instance;
const lodash = Devebot.require("lodash");
const app = require("../server");
const DBHelper = require("../utils/db-util");

// CHAI
const chai = require("chai");
const chaiHttp = require("chai-http");
const chaiAsPromised = require("chai-as-promised");
const assert = require("liberica").chai.use(chaiAsPromised).assert;
const expect = chai.expect;
chai.use(chaiHttp);

// Service
const getService = require("../utils/get-service");

// Config database
const CONNECT_ARGS = {
  host: "localhost",
  port: "27017",
  db: "test",
  username: process.env.MONGO_FC_USERNAME,
  password: process.env.MONGO_FC_PASSWORD,
};

const ENV_CLOAK_CONFIG = {
  NODE_ENV: "test",
  LOGOLITE_FULL_LOG_MODE: "false",
  LOGOLITE_ALWAYS_MUTED: "all",
  appDatastore_mongoose_manipulator_host: CONNECT_ARGS.host,
  appDatastore_mongoose_manipulator_port: CONNECT_ARGS.port,
  appDatastore_mongoose_manipulator_db: CONNECT_ARGS.db,
  appDatastore_mongoose_manipulator_username: CONNECT_ARGS.username,
  appDatastore_mongoose_manipulator_password: CONNECT_ARGS.password,
};

async function filestoreTest(app, dbUtil) {
  let args, defaultArgs, filestoreService;

  before(async function () {
    envcloak.setup(ENV_CLOAK_CONFIG);
    chores.clearCache();
    filestoreService = await getService(app, "filestoreService");
    await dbUtil.start();
  });

  beforeEach(async function () {
    const tags = [
      {
        name: 'Black'
      },
      {
        name: 'White'
      }
    ];

    const files = [
      {
        fileId: "a92e0edb-4be7-4fab-89d7-12ef79d76494",
        name: "download(1).jpeg",
        path: "/Users/lvk/Documents/Work/intro/server/data/a92e0edb-4be7-4fab-89d7-12ef79d76494/download(1).jpeg",
        size: 9430,
        status: "ok",
        type: "image/jpeg",
        fileUrl: "/filestore/download/a92e0edb-4be7-4fab-89d7-12ef79d76494",
        tags: [],
      },
      {
        fileId: "a92e0edb-4be7-4fab-89d7-12ef79d76495",
        name: "download(2).jpeg",
        path: "/Users/lvk/Documents/Work/intro/server/data/a92e0edb-4be7-4fab-89d7-12ef79d76495/download(2).jpeg",
        size: 9430,
        status: "ok",
        type: "image/jpeg",
        fileUrl: "/filestore/download/a92e0edb-4be7-4fab-89d7-12ef79d76495",
        tags: [],
      }
    ];
    
    await dbUtil.insertCollections({
      files: files,
      tags: tags
    });

    const file = await dbUtil.getCollection('files').findOne({ name: "download(1).jpeg" });
    const file1 = await dbUtil.getCollection('files').find({}).toArray();

    const tag = await dbUtil.getCollection('tags').findOne({ name: 'Black' });
    const tag1 = await dbUtil.getCollection('tags').find({}).toArray();

    const tagIds = lodash.map(tag1, (tag) => {
      return tag._id.toString();
    });

    args = {
      tags: tagIds,
      id: file._id.toString()
    }

  });

  afterEach(async function () {
    await dbUtil.cleanup();
  });

  after(async function () {
    envcloak.reset();
    chores.clearCache();
    // await dbUtil.cleanup();
    await dbUtil.stop();
  });

  it("Check all fields the file is required", async function () {
    const resData = await filestoreService.addTag(args);
    resData.data.tags = lodash.map(resData.data.tags, (tag) => {
      return tag.toString();
    });

    const expected = {
      id: args.id,
      deleted: false,
      fileId: "a92e0edb-4be7-4fab-89d7-12ef79d76494",
      name: "download(1).jpeg",
      path: "/Users/lvk/Documents/Work/intro/server/data/a92e0edb-4be7-4fab-89d7-12ef79d76494/download(1).jpeg",
      size: 9430,
      status: "ok",
      type: "image/jpeg",
      fileUrl: "/filestore/download/a92e0edb-4be7-4fab-89d7-12ef79d76494",
      tags: args.tags,
    };

    assert.deepEqual(resData.data, expected);
  });


}

describe("---> FILESTORE ENHANCE SERVICE", function () {
  this.timeout(20000);
  const dbUtil = new DBHelper(CONNECT_ARGS, CONNECT_ARGS.db);

  describe("===> TEST", function () {
    filestoreTest(app, dbUtil);
  });

});
