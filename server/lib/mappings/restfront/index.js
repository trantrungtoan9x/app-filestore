"use strict";

const filestoreRest = require("./filestore-rest");
const tagRest = require("./tag-rest");

const mappings = [
    ...filestoreRest,
    ...tagRest,
];

module.exports = mappings;
