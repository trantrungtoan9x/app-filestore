"use strict";

module.exports = [
  {
    path: ['/tags'],
    method: "GET",
    serviceName: "application/tagService",
    methodName: "getTags",
    input: {
      transform: function transform(req) {
        return {
          _start: req.query._start,
          _end: req.query._end - 1,
          _sort: req.query._sort,
          _order: req.query._order,
          q: req.query.q,
        };
      }
    },
    output: {
      transform: function transform(response) {
        return {
          headers: {
            "Access-Control-Expose-Headers": "X-Total-Count",
            "X-Total-Count": response.total
          },
          body: response.data
        };
      }
    }
  }, 
  {
    path: ['/tags/:id'],
    method: "GET",
    serviceName: "application/tagService",
    methodName: "getTag",
    input: {
      transform: function transform(req) {
        return {
          id: req.params.id,
        };
      }
    },
    output: {
      transform: function transform(response) {
        return {
          headers: {
            "Access-Control-Expose-Headers": "X-Total-Count",
            // "X-Total-Count": response.total
          },
          body: response.data
        };
      }
    }
  }, 
  {
    path: ['/tags'],
    method: "POST",
    serviceName: "application/tagService",
    methodName: "addTags",
    input: {
      transform: function transform(req) {
        return {
          name: req.body.name
        };
      }
    },
    output: {
      transform: function transform(response) {
        return {
          headers: {
            "Access-Control-Expose-Headers": "X-Total-Count",
            // "X-Total-Count": 1
          },
          body: response.data
        };
      }
    }
  }, 
  {
    path: ['/tags/:id'],
    method: "PUT",
    serviceName: "application/tagService",
    methodName: "updateTag",
    input: {
      transform: function transform(req) {
        return {
          name: req.body.name,
          id: req.params.id,
        };
      }
    },
    output: {
      transform: function transform(response) {
        return {
          headers: {
            "Access-Control-Expose-Headers": "X-Total-Count",
            // "X-Total-Count": 1
          },
          body: response.data
        };
      }
    }
  }, 
  {
    path: ['/tags/:id'],
    method: "DELETE",
    serviceName: "application/tagService",
    methodName: "deleteTag",
    input: {
      transform: function transform(req) {
        return {
          id: req.params.id,
        };
      }
    },
    output: {
      transform: function transform(response) {
        return {
          headers: {
            "Access-Control-Expose-Headers": "X-Total-Count",
            // "X-Total-Count": response.total
          },
          body: response.data
        };
      }
    }
  }, 
];