"use strict";

const mongoose = require('app-datastore').require('mongoose');
const Schema = mongoose.Schema;

module.exports = {
  name: "FilestoreModel",
  descriptor: {
    fileId: { type: String },
    name: { type: String },
    path: { type: String },
    size: { type: Number },
    status: { type: String },
    type: { type: String },
    fileUrl: { type: String },
    tags: [{ type: Schema.Types.ObjectId, ref: 'TagModel' }],
    createdAt: { type: Date },
    downloadedAt: { type: Date },
    deleted: { type: Boolean , default: false },
  },
  options: {
    collection: "files",
  },
};
