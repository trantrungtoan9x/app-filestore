"use strict";

const mongoose = require('app-datastore').require('mongoose');
const Schema = mongoose.Schema;

module.exports = {
  name: "TagModel",
  descriptor: {
    name: { type: String, require: true, unique: true },
    deleted: { type: Boolean, default: false },
    // files: [{ type: Schema.Types.ObjectId, ref: 'FilestoreModel' }]
  },
  options: {
    collection: "tags",
    timestamps: true,
  },
};