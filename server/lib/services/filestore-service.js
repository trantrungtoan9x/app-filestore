"use strict";

const Devebot = require("devebot");
const Promise = Devebot.require("bluebird");
const lodash = Devebot.require("lodash");

const sizeImage = require("image-size");
const path = require("path");

const fileValidate = require('../../schema/file-schema');

function Service(params = {}) {
  const { filestoreDao, errorBuilder } = params;

  // Get All Files
  this.getFiles = async function (args, opts = {}) {
    const sort = args._sort === 'id' ? "_id" : args._sort;
    const { files, total } = await filestoreDao.find(args._start, args._end, sort, args._order, args.q, args.tags, args.createdAt_from, args.downloadedAt_from, args.createdAt_to, args.downloadedAt_to);

    if (lodash.isEmpty(files) || lodash.isNil(files)) {
      return {
        data: [],
        total: 0,
      }
    }

    const filesConverted = lodash.map(files, (file) => {
      const resData = file.toJSON();
      resData.id = resData._id.toString();
      delete resData._id;

      resData.tags = lodash.map(resData.tags, (tag) => {
        tag.id = tag._id.toString();
        delete tag._id;

        return tag;
      });

      const dimensions = sizeImage(resData.path);
      resData.width = dimensions.width;
      resData.height = dimensions.height;

      resData.size = Math.round(resData.size / 1024);

      return resData; 
    });

    return {
      data: filesConverted,
      total: total,
    };
  };

  // Get One File
  this.getFile = async function (args, opts = {}) {
    const file = await filestoreDao.findOne(args.id);

    if (lodash.isEmpty(file)) {
      return {
        data: {},
      }
    }

    const resData = file.toJSON();
    resData.id = resData._id.toString();
    delete resData._id;

    const dimensions = sizeImage(resData.path);
    resData.width = dimensions.width;
    resData.height = dimensions.height;

    resData.size = Math.round(resData.size / 1024);

    return {
      data: resData,
    };
  };

  // Add tag
  this.addTag = async function (args, opts = {}) {
    const validated = fileValidate(args);
    const valid = validated(args);

    if (!valid) {
      return {
        data: {
          msg: validated.errors
        }
      }
    }

    const file = await filestoreDao.findOne(args.id);

    if (lodash.isEmpty(file)) { 
      return {
        data: {},
      }
    }
    const fileUpdate = await filestoreDao.update(args.id, lodash.pick(args, ["tags"]));

    const filesConverted = fileUpdate.toJSON();
    filesConverted.id = filesConverted._id.toString();
    delete filesConverted._id;

    return {
      data: filesConverted,
    };
  };

  // Delete File By Id
  this.deleteFileById = async function (args, opts = {}) {
    const msg = await filestoreDao.delete(args.id);

    return {
      data: msg,
    };
  };
}

Service.referenceHash = {
  filestoreDao: "filestoreDao",
  errorBuilder: "errorBuilder",
};

module.exports = Service;
