"use strict";

const Devebot = require("devebot");
const Promise = Devebot.require("bluebird");
const lodash = Devebot.require("lodash");

const fs = require("fs");
const path = require("path");

function Service(params = {}) {
  const { tagDao, filestoreDao, errorBuilder } = params;

  // Select All TAG
  this.getTags = async function (args, opts = {}) {
    const sort = args._sort === 'id' ? '_id' : args._sort;
    const { tags, total } = await tagDao.find(args._start, args._end, sort, args._order, args.q);

    if (lodash.isEmpty(tags)) {
      return {
        data: [],
        total: 0,
      }
    }

    const tagConverted = lodash.map(tags, (tag) => {
      const resData = tag.toJSON();
      resData.id = tag._id;
      delete resData._id;

      const data = lodash.pick(resData, ['id', 'name', 'createdAt', 'updatedAt']);

      return data;
    });

    return {
      data: tagConverted,
      total: total,
    };
  }

  // Select One TAG
  this.getTag = async function (args, opts = {}) {
    const tag = await tagDao.findOne(args.id);

    if (lodash.isEmpty(tag)) {
      return {
        data: {},
      }
    }

    const tagConverted = tag.toJSON();
    tagConverted.id = tag._id;
    delete tagConverted._id;

    return {
      data: tagConverted,
    }
  }

  // create TAG
  this.addTags = async function (args, opts = {}) {
    const tag = await tagDao.create(args);

    const tagConverted = tag.toJSON();
    tagConverted.id = tagConverted._id.toString();
    delete tagConverted._id;

    return {
      data: tagConverted
    }
  }

  // Update TAG
  this.updateTag = async function (args, opts = {}) {
    const tag = await tagDao.findOne(args.id);

    if (lodash.isEmpty(tag)) {
      return {
        data: {},
      }
    }
    const tagUpdate = await tagDao.update(args.id, lodash.pick(args, ['name']));

    const tagConverted = tagUpdate.toJSON();
    tagConverted.id = tagConverted._id.toString();
    delete tagConverted._id;

    return {
      data: tagConverted,
    }
  }

  // Delete TAG
  this.deleteTag = async function (args, opts = {}) {
    const tag = await tagDao.findOne(args.id);
    if (lodash.isEmpty(tag)) {
      return {
        data: {},
      }
    }
    const tagDelete = await tagDao.delete(args.id);

    const files = await filestoreDao.findByTag(args.id);
    
    lodash.forEach(files, async (file) => {
      const filesConverted = file.toJSON();
      filesConverted.tags = lodash.filter(filesConverted.tags, (tag) => {
        return tag != args.id;
      });

      await filestoreDao.update(filesConverted._id, lodash.pick(filesConverted, ['tags']));
    });

    return {
      data: tag
    };
  }

}

Service.referenceHash = {
  tagDao: "tagDao",
  filestoreDao: 'filestoreDao',
  errorBuilder: 'errorBuilder'
};

module.exports = Service;