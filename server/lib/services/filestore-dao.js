"use strict";

const Devebot = require("devebot");
const Promise = Devebot.require("bluebird");
const lodash = Devebot.require("lodash");

function Service(params = {}) {
  const { dataManipulator } = params;

  this.find = async function (start, end, sort = `_id`, order = `DESC`, q, tags, createdAt_from, downloadedAt_from, createdAt_to, downloadedAt_to) {
    let query = { 
      // deleted: false
    };

    if (!lodash.isEmpty(q)) {
      lodash.assign(query, {
        $or: [
          { name: { $regex: q } },
        ]
      });
    }

    if (!lodash.isEmpty(tags)) {
      lodash.assign(query, {
        tags: { $all: tags }
      });
    }

    if (!lodash.isEmpty(createdAt_from)) {
      lodash.assign(query, {
        createdAt: { $gte: createdAt_from.toString() },
      });
    }

    if (!lodash.isEmpty(createdAt_to)) {
      lodash.assign(query?.createdAt, {
        $lte: createdAt_to.toString() 
      });
    }

    // if (!lodash.isEmpty(createdAt_from) && !lodash.isEmpty(createdAt_to)) {
    //   query = {
    //     createdAt: {
    //       $gte: createdAt_from.toString(),
    //       $lte: createdAt_to.toString(),
    //     }
    //   }
    // }

    if (!lodash.isEmpty(downloadedAt_from)) {
      lodash.assign(query, {
        downloadedAt: { $gte: downloadedAt_from.toString() },
      });
    }

    if (!lodash.isEmpty(downloadedAt_to)) {
      lodash.assign(query?.downloadedAt, {
        $lte: downloadedAt_to.toString()
      });
    }

    // if (!lodash.isEmpty(downloadedAt_from) && !lodash.isEmpty(downloadedAt_to)) {
    //   query = {
    //     downloadedAt: {
    //       $gte: downloadedAt_from.toString(),
    //       $lte: downloadedAt_to.toString(),
    //     }
    //   }
    // }

    // if (!lodash.isEmpty(createdAt_from) && !lodash.isEmpty(createdAt_to) && !lodash.isEmpty(downloadedAt_from) && !lodash.isEmpty(downloadedAt_to)) {
    //   query = {
    //     downloadedAt: {
    //       $gte: downloadedAt_from.toString(),
    //       $lte: downloadedAt_to.toString(),
    //     },
    //     createdAt: {
    //       $gte: createdAt_from.toString(),
    //       $lte: createdAt_to.toString(),
    //     }
    //   }
    // }

    const files = await dataManipulator.find({
      type: 'FilestoreModel',
      query: query,
      options: {
        skip: parseInt(start),
        limit: parseInt((end - start) + 1),
        sort: {
          [sort]: (order === 'ASC') ? 1 : -1, // tăng dần 
        }
      },
      populates: [
        {
          path: 'tags',
          select: ['name']
        }
      ]
    });

    const total = await dataManipulator.count({
      type: 'FilestoreModel',
      filter: query,
    });

    return { files, total };
  }

  this.findOne = async function (id) {
    const file = await dataManipulator.findOne({ 
      type: 'FilestoreModel', 
      query: { _id: id },
      // populates: [
      //   {
      //     path: 'tags',
      //     select: ['name']
      //   }
      // ]
    });

    return file;
  }

  this.findByTag = async function (tag) {
    const files = await dataManipulator.find({
      type: 'FilestoreModel',
      query: {
        tags: { $in: tag }
      }
    });

    return files;
  }

  this.update = async function (id, data) {
    await dataManipulator.update({ type: 'FilestoreModel', _id: id, data: data });

    const file = await this.findOne(id);

    return file;
  }

  this.delete = async function (id) {
    await dataManipulator.update({ type: 'FilestoreModel', _id: id, data: { deleted: true } });

    return { msg: 'Successfully deleted database' };
  }

  this.deleteFile = async function (id) {
    const file = await filestoreDao.findOne(id);
    const dir = path.join(__dirname, "../../data", file.fileId);

    await fs.rmdir(dir, { recursive: true }, (err) => {
      if (err) {
        console.log(err);
        return;
      }

      console.log("Successfully deleted fileId:  " + file.fileId);
    });

    return { msg: "Successfully deleted file" }
  }

  this.count = async function () {
    const count = await dataManipulator.count({ type: 'FilestoreModel' });

    return count;
  }
}

Service.referenceHash = {
  dataManipulator: "app-datastore/dataManipulator",
};

module.exports = Service;
