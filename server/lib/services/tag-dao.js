"use strict";

const Devebot = require("devebot");
const Promise = Devebot.require("bluebird");
const lodash = Devebot.require("lodash");

function Service(params = {}) {
  const { dataManipulator, filestoreDao } = params;

  this.create = async function (data) {
    const tag = await dataManipulator.create({ type: 'TagModel', data: data });

    return tag;
  }

  this.find = async function (start, end, sort = '_id', order = 'DESC', q) {
    let query = {
      deleted: false,
    };

    if (!lodash.isNil(q)) {
      query = {
        deleted: false,
        name: { $regex: q }
      };
    }
    const tags = await dataManipulator.find({ 
      type: 'TagModel', 
      query: query,
      options: {
        skip: parseInt(start),
        limit: parseInt((end - start) + 1),
        sort: {
          [sort]: (order === 'ASC') ? 1 : -1,
        }
      }
    });

    const total = await dataManipulator.count({
      type: 'TagModel',
      filter: query,
    });

    return { tags, total };
  }

  this.findOne = async function (id) {
    const tag = await dataManipulator.findOne({ type: 'TagModel', query: { _id: id, deleted: false } });

    return tag;
  }

  this.findOneDelete = async function (id) {
    const tag = await dataManipulator.findOne({ type: 'TagModel', query: { _id: id, deleted: true } });

    return tag;
  }

  this.update = async function (id, data) {
    await dataManipulator.update({ type: 'TagModel', _id: id, data: data });
    const tag = this.findOne(id);

    return tag;
  }

  this.delete = async function (id) {
    const tag = await dataManipulator.update({ type: 'TagModel', _id: id, data: { deleted: true } });

    return tag;
  }

  this.count = async function (query) {
    const count = await dataManipulator.count({ type: 'TagModel' });

    return count;
  }



}

Service.referenceHash = {
  dataManipulator: "app-datastore/dataManipulator",
  filestoreDao: "filestoreDao",
};

module.exports = Service; 