"use strict";

module.exports = {
    GeneralError: {
        message: "Có lỗi xảy ra",
        returnCode: 500,
        statusCode: 400,
    },

    EmptyError: {
        message: "Lỗi trống dữ liệu",
        returnCode: 500,
        statusCode: 400,
    },

    NotFoundError: {
        message: "Lỗi không tìm thấy",
        returnCode: 404,
        statusCode: 404,
    },

    ValidateError: {
        message: "Lỗi dữ liệu",
        returnCode: 500,
        statusCode: 500,
    },
    DuplicateError: {
        message: "Trùng dữ liệu",
        returnCode: 500,
        statusCode: 500,
    },
    
};
