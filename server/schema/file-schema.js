const Ajv = require('ajv');
const ajvFormat = require('ajv-formats');

const ajv = new Ajv({ allErrors: true });

ajvFormat(ajv);

const fileValidate = (args) => {
  const schema = {
    type: 'object',
    properties: {
      tags: {
        type: 'array',
      }
    },
    required: ['tags'],
  }

  return ajv.compile(schema);
}

module.exports = fileValidate;
