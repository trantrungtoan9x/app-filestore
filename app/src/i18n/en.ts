import { TranslationMessages } from "react-admin";
import englishMessages from "ra-language-english";

const customEnglishMessages: TranslationMessages = {
  ...englishMessages,
  pos: {
    search: "Search name",
    configuration: "Configuration",
    language: "Language",
    theme: {
      name: "Theme",
      light: "Light",
      dark: "Dark",
    },
    menu: {
      category: "Category",
      images: "Images",
      tag: "Tags",
    },
  },
  resources: {
    files: {
      fileId: "Images",
      fields: {
        id: "ID",
        name: "Name",
        size: "Size",
        tags: "Tags",
        createdAt: 'Create Time',
        downloadedAt: 'Download Lastest Time',
        createdAt_from: "Created at from",
        createdAt_to: "Created at from",
        downloadedAt_from: "Downloaded at from",
        downloadedAt_to: "Downloaded at to"

      },
      filters: {
        last_visited: "Last visited",
        today: "Today",
        this_week: "This week",
        last_week: "Last week",
        this_month: "This month",
        last_month: "Last month",
        earlier: "Earlier",
        has_ordered: "Has ordered",
        has_newsletter: "Has newsletter",
        group: "Segment",
      },
      fieldGroups: {
        identity: "Identity",
        address: "Address",
        stats: "Stats",
        history: "History",
        password: "Password",
        change_password: "Change Password",
      },
      page: {
        delete: "Delete Customer",
      },
      errors: {
        password_mismatch:
          "The password confirmation is not the same as the password.",
      },
    },
    tags: {
      name: "Tag",
      fields: {
        name: "Name Tag",
        createdAt: "Created At",
        updatedAt: "Updated At",
      },
    },
  },
};

export default customEnglishMessages;
