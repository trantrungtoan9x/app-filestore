import { TranslationMessages } from "react-admin";
import frenchMessages from "ra-language-french";

const customFrenchMessages: TranslationMessages = {
  ...frenchMessages,
  pos: {
    dashboard: "",
    search: "Tìm kiếm theo tên",
    configuration: "Cấu hình",
    language: "Ngôn ngữ",
    theme: {
      name: "Theme",
      light: "Light",
      dark: "Dark",
    },
    menu: {
      category: "Loại",
      images: "Hình ảnh",
      tag: "Thẻ",
    },
  },
  resources: {
    files: {
      fileId: "Hình ảnh",
      fields: {
        id: "ID",
        name: "Tên",
        size: "Kích thước",
        tags: "Thẻ",
        createdAt: "Ngày tạo",
        downloadedAt: "Tải xuống lần cuối cùng",
        createdAt_from: "Ngày tạo từ",
        createdAt_to: "Ngày tạo đến",
        downloadedAt_from: "Tải xuống từ",
        downloadedAt_to: "Tải xuống đến",
      },
      filters: {
        last_visited: "Last visited",
        today: "Today",
        this_week: "This week",
        last_week: "Last week",
        this_month: "This month",
        last_month: "Last month",
        earlier: "Earlier",
        has_ordered: "Has ordered",
        has_newsletter: "Has newsletter",
        group: "Segment",
      },
      fieldGroups: {
        identity: "Identity",
        address: "Address",
        stats: "Stats",
        history: "History",
        password: "Password",
        change_password: "Change Password",
      },
      page: {
        delete: "Delete Customer",
      },
      errors: {
        password_mismatch:
          "The password confirmation is not the same as the password.",
      },
    },
    tags: {
      name: "Thẻ",
      fields: {
        name: "Tên thẻ",
        createdAt: "Ngày tạo",
        updatedAt: "Ngày cập nhật",
      },
    },
  },
};

export default customFrenchMessages;
