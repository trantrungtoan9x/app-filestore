import * as React from 'react'
import {
  AppBar,
  Logout,
  UserMenu,
  useTranslate
} from 'react-admin'
import { Link } from 'react-router-dom'
import {
  MenuItem,
  ListItemIcon,
  ListItemText,
} from '@mui/material'
import SettingsIcon from '@mui/icons-material/Settings'

const ConfigurationMenu = React.forwardRef((props, ref) => {
  const translate = useTranslate()
  return (
    <MenuItem
      component={Link}
      // @ts-ignore
      ref={ref}
      {...props}
      to="/configuration"
    >
      <ListItemIcon>
        <SettingsIcon />
      </ListItemIcon>
      <ListItemText>{translate('pos.configuration')}</ListItemText>
    </MenuItem>
  )
})
const CustomUserMenu = () => (
  <UserMenu>
    <ConfigurationMenu />
    <Logout />
  </UserMenu>
)

const CustomAppBar = (props: any) => {

  return (
    <AppBar
      {...props}
      color="secondary"
      elevation={1}
      userMenu={<CustomUserMenu />}
    >
    </AppBar>
  )
}

export default CustomAppBar
