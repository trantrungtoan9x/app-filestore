import * as React from 'react'
import { Card, CardMedia, Box, Typography } from '@mui/material'
import { useRecordContext } from 'react-admin'
import { Product } from '../types'
import { API } from '../App'

const Poster = () => {
  const record = useRecordContext<Product>()

  if (!record) return null
  return (
    <Box>
      <Card
        sx={{
          display: 'flex',
          maxWidth: '100%',
          maxHeight: '400px',
          objectFit: 'cover',
          boxShadow: 'none'
        }}
      >
        <CardMedia
          component="img"
          src={`${API}filestore/picture/${record.fileId}/${record.width}/${record.height}/${record.name}`}
          alt={record.name}
          sx={{ objectFit: 'contain' }}
        />
      </Card>
      <Typography
        component="div"
        children={`${record.width}x${record.height}`}
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          color: '#8B8B8B'
        }}
      />
    </Box>
  )
}

export default Poster
