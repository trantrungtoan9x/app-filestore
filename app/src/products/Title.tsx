import { useRecordContext } from "react-admin";
import { Product } from "../types";

const Title = () => {
  const record = useRecordContext<Product>();
  return record ? <span>Image "{record.name}"</span> : null;
};

export default Title;
