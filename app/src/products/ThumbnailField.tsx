import { useRecordContext } from "react-admin";
import { Product } from "../types";
import { API } from "../App";
import { PhotoProvider, PhotoView } from "react-photo-view";
import "react-photo-view/dist/react-photo-view.css";

const ThumbnailField = (props: { source?: string; label?: string }) => {
  const record = useRecordContext<Product>();

  return (
    <PhotoProvider
      loop={4}
      speed={() => 800}
      easing={(type) =>
        type === 2
          ? "cubic-bezier(0.36, 0, 0.66, -0.56)"
          : "cubic-bezier(0.34, 1.56, 0.64, 1)"
      }
    >
      <PhotoView
        // width={300}
        src={`${API}filestore/picture/${record.fileId}/${record.width}/${record.height}/${record.name}`}
      >
        <img
          src={`${API}filestore/picture/${record.fileId}/${record.width}/${record.height}/${record.name}`}
          style={{
            width: 240,
            height: 135,
            objectFit: "cover",
            maxWidth: 360,
            maxHeight: 270,
            // verticalAlign: "middle" ,
          }}
          alt=""
        />
      </PhotoView>
    </PhotoProvider>
  );
};

export default ThumbnailField;
