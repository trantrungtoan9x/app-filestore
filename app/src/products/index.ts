import ProductEdit from './ProductEdit'
import { ProductList } from './ProductList'
import ProductShow from './ProductShow'
import InsertPhotoIcon from '@mui/icons-material/InsertPhoto'

const Product = {
  list: ProductList,
  edit: ProductEdit,
  show: ProductShow,
  icon: InsertPhotoIcon
}

export default Product;