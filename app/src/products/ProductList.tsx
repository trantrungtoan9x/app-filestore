import * as React from "react";
import {
  AutocompleteArrayInput,
  Datagrid,
  DateField,
  DateInput,
  EditButton,
  List,
  Pagination,
  ReferenceInput,
  ShowButton,
  TextField,
  TextInput,
} from "react-admin";
import { Tag } from "../types";
import SegmentsField from "./SegmentsField";
import ThumbnailField from "./ThumbnailField";

const PostPagination = () => (
  <Pagination rowsPerPageOptions={[5, 10, 25, 50]} />
);
const postFilters = [
  <TextInput label="pos.search" source="q" variant="outlined" alwaysOn />,
  <ReferenceInput source="tags" reference="tags">
    <AutocompleteArrayInput
      optionText={(choice?: Tag) => (choice?.id ? `${choice.name} ` : "")}
    />
  </ReferenceInput>,
  <DateInput source="createdAt_from" />,
  <DateInput source="createdAt_to" />,
  <DateInput source="downloadedAt_from" />,
  <DateInput source="downloadedAt_to" />,
];

export const ProductList = () => (
  <List
    disableSyncWithLocation={false}
    exporter={false}
    perPage={5}
    pagination={<PostPagination />}
    filters={postFilters}
  >
    <Datagrid>
      <ThumbnailField/>
      <TextField source="id" textAlign="center" emptyText="-" />
      <TextField source="name" textAlign="center" emptyText="-" />
      <TextField source="size" textAlign="center" emptyText="-" />
      <SegmentsField source="tags" textAlign="center" />
      <DateField
        source="createdAt"
        textAlign="center"
        locales="fr-FR"
        showTime
        emptyText="-"
      />
      <DateField
        source="downloadedAt"
        textAlign="center"
        locales="fr-FR"
        showTime
        emptyText="-"
      />
      <ShowButton label="" />
      <EditButton label="" />
    </Datagrid>
  </List>
);
