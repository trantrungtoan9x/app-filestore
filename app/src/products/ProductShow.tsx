import { Grid, Typography, Box } from '@mui/material'
import {
  ChipField,
  DateField,
  ReferenceArrayField,
  Show,
  SingleFieldList,
  Tab,
  TabbedShowLayout,
  TextField,
} from 'react-admin'
import Poster from './Poster'
import Title from './Title';

const ProductShow = () => (
  <Show title={<Title />}>
    <TabbedShowLayout>
      <Tab label="Detail">
        <Grid container spacing={2}>
          <Grid item md={6}>
            <Poster />
          </Grid>
          <Grid item xs={6}>
            <Box display="flex" alignItems="center">
              <Grid item xs={3}>
                <Typography
                  fontWeight={600}
                  variant="subtitle2"
                  component="div"
                  children="Id"
                  fontSize={20}
                  m={1}
                />
              </Grid>
              <Grid item xs={9}>
                <TextField source="id" fontSize={20} fullWidth />
              </Grid>
            </Box>
            <Box display="flex" alignItems="center">
              <Grid item xs={3}>
                <Typography
                  fontWeight={600}
                  variant="subtitle2"
                  component="div"
                  children="Name"
                  fontSize={20}
                  m={1}
                />
              </Grid>
              <Grid item xs={9}>
                <TextField source="name" fontSize={20} fullWidth />
              </Grid>
            </Box>
            <Box display="flex" alignItems="center">
              <Grid item xs={3}>
                <Typography
                  fontWeight={600}
                  variant="subtitle2"
                  component="div"
                  children="File size"
                  fontSize={20}
                  m={1}
                />
              </Grid>
              <Grid item xs={9}>
                <TextField source="size" fontSize={20} fullWidth />
              </Grid>
            </Box>
            <Box display="flex" alignItems="center">
              <Grid item xs={3}>
                <Typography
                  fontWeight={600}
                  variant="subtitle2"
                  component="div"
                  children="Type"
                  fontSize={20}
                  m={1}
                />
              </Grid>
              <Grid item xs={9}>
                <TextField source="type" fontSize={20} fullWidth />
              </Grid>
            </Box>
            <Box display="flex" alignItems="center">
              <Grid item xs={3}>
                <Typography
                  fontWeight={600}
                  variant="subtitle2"
                  component="div"
                  children="Created At"
                  fontSize={20}
                  m={1}
                />
              </Grid>
              <Grid item xs={9}>
                <DateField
                  source="createdAt"
                  showTime
                  fontSize={20}
                  fullWidth
                />
              </Grid>
            </Box>
            <Box display="flex" alignItems="center">
              <Grid item xs={3}>
                <Typography
                  fontWeight={600}
                  variant="subtitle2"
                  component="div"
                  children="Downloaded At"
                  fontSize={20}
                  m={1}
                />
              </Grid>
              <Grid item xs={9}>
                <DateField
                  source="downloadedAt"
                  showTime
                  fontSize={20}
                  fullWidth
                />
              </Grid>
            </Box>
            <Box display="flex" alignItems="center">
              <Grid item xs={3}>
                <Typography
                  fontWeight={600}
                  variant="subtitle2"
                  component="div"
                  children="Tags"
                  fontSize={20}
                  m={1}
                />
              </Grid>
              <Grid item xs={9}>
                <ReferenceArrayField
                  label="Tags"
                  reference="tags"
                  source="tags"
                >
                  <SingleFieldList>
                    <ChipField
                      source="name"
                      color="primary"
                      variant="outlined"
                    />
                  </SingleFieldList>
                </ReferenceArrayField>
              </Grid>
            </Box>
          </Grid>
        </Grid>
      </Tab>
    </TabbedShowLayout>
  </Show>
)

export default ProductShow
