import { Grid, Typography, Box } from "@mui/material";
import * as React from "react";
import {
  AutocompleteArrayInput,
  DateField,
  Edit,
  FormTab,
  ReferenceArrayInput,
  required,
  SaveButton,
  TabbedForm,
  TextField,
  Toolbar,
  useRedirect,
} from "react-admin";
import Poster from "./Poster";
import { Button } from "@mui/material";
import Title from "./Title";

const ToolbarEdit = () => {
  const redirect = useRedirect();
  const handleClickCancel = () => {
    redirect("/files");
  };
  return (
    <Toolbar sx={{ display: "flex", justifyContent: "space-between" }}>
      <Button
        children="Cancel"
        onClick={handleClickCancel}
        variant="contained"
        color="success"
      />
      <SaveButton />
    </Toolbar>
  );
};

const ProductEdit = () => {
  return (
    <Edit title={<Title />} redirect="edit">
      <TabbedForm toolbar={<ToolbarEdit />}>
        <FormTab label="image">
          <Grid container spacing={2}>
            <Grid item sm={6} xs={12}>
              <Poster />
            </Grid>
            <Grid item sm={6} xs={12}>
              <Box display="flex" alignItems="center">
                <Grid item xs={3}>
                  <Typography
                    fontWeight={600}
                    variant="subtitle2"
                    component="div"
                    children="Id"
                    fontSize={20}
                    m={1}
                  />
                </Grid>
                <Grid item xs={9}>
                  <TextField source="id" fontSize={20} fullWidth />
                </Grid>
              </Box>
              <Box display="flex" alignItems="center">
                <Grid item xs={3}>
                  <Typography
                    fontWeight={600}
                    variant="subtitle2"
                    component="div"
                    children="Name"
                    fontSize={20}
                    m={1}
                  />
                </Grid>
                <Grid item xs={9}>
                  <TextField source="name" fontSize={20} fullWidth />
                </Grid>
              </Box>
              <Box display="flex" alignItems="center">
                <Grid item xs={3}>
                  <Typography
                    fontWeight={600}
                    variant="subtitle2"
                    component="div"
                    children="File size"
                    fontSize={20}
                    m={1}
                  />
                </Grid>
                <Grid item xs={9}>
                  <TextField source="size" fontSize={20} fullWidth />
                </Grid>
              </Box>
              <Box display="flex" alignItems="center">
                <Grid item xs={3}>
                  <Typography
                    fontWeight={600}
                    variant="subtitle2"
                    component="div"
                    children="Type"
                    fontSize={20}
                    m={1}
                  />
                </Grid>
                <Grid item xs={9}>
                  <TextField source="type" fontSize={20} fullWidth />
                </Grid>
              </Box>
              <Box display="flex" alignItems="center">
                <Grid item xs={3}>
                  <Typography
                    fontWeight={600}
                    variant="subtitle2"
                    component="div"
                    children="Created At"
                    fontSize={20}
                    m={1}
                  />
                </Grid>
                <Grid item xs={9}>
                  <DateField
                    source="createdAt"
                    showTime
                    fontSize={20}
                    fullWidth
                  />
                </Grid>
              </Box>
              <Box display="flex" alignItems="center">
                <Grid item xs={3}>
                  <Typography
                    fontWeight={600}
                    variant="subtitle2"
                    component="div"
                    children="Downloaded At"
                    fontSize={20}
                    m={1}
                  />
                </Grid>
                <Grid item xs={9}>
                  <DateField
                    source="downloadedAt"
                    showTime
                    fontSize={20}
                    fullWidth
                  />
                </Grid>
              </Box>
              <Box display="flex" alignItems="center">
                <Grid item sm={3}>
                  <Typography
                    fontWeight={600}
                    variant="subtitle1"
                    children="Tags"
                    fontSize={20}
                    m={1}
                  />
                </Grid>
                <Grid item sm={9}>
                  <ReferenceArrayInput source="tags" reference="tags">
                    <AutocompleteArrayInput
                      fullWidth
                      label={false}
                      variant="standard"
                      source="tags"
                      optionText="name"
                      optionValue="id"
                      validate={req}
                    />
                  </ReferenceArrayInput>
                </Grid>
              </Box>
            </Grid>
          </Grid>
        </FormTab>
      </TabbedForm>
    </Edit>
  );
};

const req = [required()];

export default ProductEdit;
