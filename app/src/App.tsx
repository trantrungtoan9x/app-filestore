import { Admin, CustomRoutes, Resource } from 'react-admin'
import './App.css'
import Dashboard from './Dashboard'
import jsonServerProvider from 'ra-data-json-server'
import products from './products'
import tags from './tags'
import { Layout } from './Layout'
import englishMessages from './i18n/en'
import polyglotI18nProvider from 'ra-i18n-polyglot'
import { Route } from 'react-router'
import Configuration from './configuration/Configuration'
import NotFound from './NotFound'

export const API = 'http://192.168.2.37:7979/'

// const dataProvider = jsonServerProvider('http://localhost:7979')
const dataProvider = jsonServerProvider(`${API}api`)

const i18nProvider = polyglotI18nProvider(locale => {
  if (locale === 'fr') {
      return import('./i18n/fr').then(messages => messages.default);
  }
  // Always fallback on english
  return englishMessages
}, 'en')

function App() {
  return (
    <Admin
      dashboard={Dashboard}
      i18nProvider={i18nProvider}
      layout={Layout}
      dataProvider={dataProvider}
      catchAll={NotFound}
    >
      <CustomRoutes>
        <Route path="/configuration" element={<Configuration />} />
      </CustomRoutes>
      <Resource name="files" {...products} />
      <Resource name="tags" {...tags} />
    </Admin>
  )
}

export default App
