import * as React from 'react'
import {
  Edit,
  SimpleForm,
  TextInput,
  required,
  Toolbar,
  SaveButton
} from 'react-admin'
import { Typography, Grid, Box } from '@mui/material'

const ToolbarTags =()=> (
  <Toolbar>
    <SaveButton />
  </Toolbar>
)
const TagEdit = () => {
  return (
    <Box>
      <Edit title="Edit Tags">
        <SimpleForm toolbar={<ToolbarTags />}>
          <Grid
            container
            spacing={2}
            style={{
              display: 'center',
              justifyContent: 'center'
            }}
          >
            <Grid item xs={12}>
              {/* <Box display="flex" justifyContent="center">
                <Grid item xs={3}>
                  <Typography
                    fontWeight={600}
                    variant="subtitle2"
                    component="div"
                    children="Id"
                    fontSize={18}
                    m={2}
                  />
                </Grid>
                <Grid item xs={9} style={{ marginTop: '20px' }}>
                  <TextField source="id" fontSize={16} />
                </Grid>
              </Box>
              <Box display="flex" justifyContent="center">
                <Grid item xs={3}>
                  <Typography
                    fontWeight={600}
                    variant="subtitle2"
                    component="div"
                    children="Create Time"
                    fontSize={18}
                    m={2}
                  />
                </Grid>
                <Grid item xs={9} style={{ marginTop: '20px' }}>
                  <DateField showTime source="createdAt" fontSize={16} />
                </Grid>
              </Box>
              <Box display="flex" justifyContent="center">
                <Grid item xs={3}>
                  <Typography
                    fontWeight={600}
                    variant="subtitle2"
                    component="div"
                    children="Update Time"
                    fontSize={18}
                    m={2}
                  />
                </Grid>
                <Grid item xs={9} style={{ marginTop: '20px' }}>
                  <DateField showTime source="updatedAt" fontSize={16} />
                </Grid>
              </Box> */}
              <Box display="flex" alignItems="center">
                <Grid item xs={2}>
                  <Typography
                    fontWeight={600}
                    variant="subtitle2"
                    component="div"
                    children="Tag Name"
                    fontSize={18}
                    m={2}
                  />
                </Grid>
                <Grid item xs={10}>
                  <TextInput
                    source="name"
                    label={false}
                    variant="standard"
                    validate={[required()]}
                  />
                </Grid>
              </Box>
            </Grid>
          </Grid>
        </SimpleForm>
      </Edit>
    </Box>
  )
}

export default TagEdit
