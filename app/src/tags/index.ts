import { TagList } from "../tags/TagList";
import TagEdit from "./TagEdit";
const Tag = {
  list: TagList,
  edit: TagEdit,
};
 export default Tag;