import { Box } from '@mui/material'
import { Create, required, SimpleForm, TextInput } from 'react-admin'

const TagCreate = () => {
  return (
    <Box maxWidth="50em">
      <Create redirect="list">
        <SimpleForm>
          <TextInput source="name" validate={[required()]} fullWidth />
        </SimpleForm>
      </Create>
    </Box>
  )
}

export default TagCreate
