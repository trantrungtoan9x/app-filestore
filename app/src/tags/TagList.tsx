import AddIcon from "@mui/icons-material/Add";
import { Box, Button, Dialog, DialogContent, DialogTitle } from "@mui/material";
import * as React from "react";
import {
  Create,
  Datagrid,
  DateField,
  DeleteButton,
  EditButton,
  List,
  Pagination,
  required,
  SaveButton,
  SimpleForm,
  TextField,
  TextInput,
  Toolbar,
  useRefresh,
} from "react-admin";

const PostPagination = () => (
  <Pagination rowsPerPageOptions={[5, 10, 25, 50]} />
);

const TagToolbar = (props: any) => {
  const refresh = useRefresh();
  const { setOpen } = props;
  const handelOnClick = () => {
    setOpen(false);
    setTimeout(refresh, 500);
  };
  const handleClickCancel = () => {
    setOpen(false);
  };
  return (
    <Toolbar sx={{ display: "flex", justifyContent: "space-between" }}>
      <Button
        children="Cancel"
        onClick={handleClickCancel}
        variant="contained"
        color="success"
      />
      <SaveButton
        label="Save"
        type="button"
        variant="contained"
        onClick={handelOnClick}
      />
    </Toolbar>
  );
};

const TagsActions = () => {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  return (
    <Toolbar>
      <Button
        variant="outlined"
        startIcon={<AddIcon />}
        onClick={handleClickOpen}
      >
        Create
      </Button>
      <Dialog open={open}>
        <DialogTitle sx={{ width: "400px" }} justifyItems="center">
          Add Tags
        </DialogTitle>
        <DialogContent sx={{ padding: 0 }}>
          <Create redirect="list" title=" ">
            <SimpleForm toolbar={<TagToolbar setOpen={setOpen} />}>
              <TextInput source="name" validate={[required()]} fullWidth />
            </SimpleForm>
          </Create>
        </DialogContent>
      </Dialog>
    </Toolbar>
  );
};

export const TagList = () => {
  return (
    <Box>
      <List
        perPage={5}
        pagination={<PostPagination />}
        actions={<TagsActions />}
        empty={false}
      >
        <Datagrid>
          <TextField source="name" textAlign="center" />
          <DateField source="createdAt" showTime textAlign="center" />
          <DateField showTime source="updatedAt" textAlign="center" />
          <EditButton label="" />
          <DeleteButton label="" />
        </Datagrid>
      </List>
    </Box>
  );
};
